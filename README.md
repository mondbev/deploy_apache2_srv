# deploy_apache2_srv

Deploy apache2 server on deb rpm servers headless with privileged user
Sourcing # deploy_deb_rpm https://gitlab.com/mondbev/deploy_deb_rpm

# ########################################
#           Author: Shai
#           Date: 11/02/2021
#
#   Purpose: source deploy_deb_rpm and init:
#           -Install and conf webserver
#
#   Usage:  At remote server get git and clone
#           https://gitlab.com/mondbev/deploy_apache2_srv
#           Change vars to suite you or use default and [init.sh] #init.sh
# ##############################################################